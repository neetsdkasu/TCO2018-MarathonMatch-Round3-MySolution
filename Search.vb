Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On
Imports Console = System.Console

Public Module Main
    
    Dim startTime As Integer = 0
    
    Function ToTimeStr(t As Integer) As String
        Dim ms As Integer = t Mod 1000
        t = (t - ms) \ 1000
        Dim se As Integer = t Mod 60
        t = (t - se) \ 60
        Dim mn As Integer = t Mod 60
        t = (t - mn) \ 60
        ToTimeStr = String.Format("{0}:{1:D2}:{2:D2}.{3:D3}", _
            t, mn, se, ms)
    End Function
    
	Public Sub Main()
		Try
            startTime = Environment.TickCount
            
            Run()
            
            Dim endTime As Integer = Environment.TickCount
            
            Dim totalTime As Integer = endTime - startTime
            
            Console.Error.WriteLine("total time: {0}", _
                ToTimeStr(totalTime))
            
		Catch Ex As Exception
			Console.Error.WriteLine(ex)
		End Try
	End Sub
    
    Const TCCount As Integer = 500
    
    Class State
        Public MinBorder  As Integer = 81
        Public BorderRate As Integer =  9
        Public AtariGosa  As Integer = 48
        Public OkRate     As Integer = 65
        Public BureRate   As Integer = 33
        Public TCScores(TCCount - 1) As Integer
        Public Score As Double = 0.0
        Sub CopyTo(s As State)
            s.MinBorder  = MinBorder
            s.BorderRate = BorderRate
            s.AtariGosa  = AtariGosa
            s.OkRate     = OkRate
            s.BureRate   = BureRate
        End Sub
        Sub CopyAllTo(s As State)
            CopyTo(s)
            Array.Copy(TCScores, s.TCScores, TCScores.Length)
            s.Score = Score
        End Sub
        Sub CopyTo(s As InvestmentAdvice)
            s.MinBorder  = MinBorder
            s.BorderRate = BorderRate
            s.AtariGosa  = AtariGosa
            s.OkRate     = OkRate
            s.BureRate   = BureRate
            ReDim s.patterns(-1)
        End Sub
        Sub Calc(maxScores() As Integer)
            Score = 0.0
            For i As Integer = 0 To UBound(TCScores)
                If maxScores(i) > 0 Then
                    Score += CDbl(TCScores(i)) / CDbl(maxScores(i))
                End If
            Next i
        End Sub
        Public Overrides Function ToString() As String
            ToString = String.Format( _
                "{0} {1} {2} {3} {4} {5}", _
                MinBorder, BorderRate, AtariGosa, OkRate, BureRate, _
                score / CDbl(TCCount))
        End Function
    End Class
    
    Dim maxScores(TCCount - 1) As Integer
    
    Const LimitTime As Integer = 20 * 60 * 1000
    
    Sub Run()
        Console.Error.WriteLine("({0}, {1}), ({2}, {3})", _
            MinN, MaxN, MinRound, MaxRound)
        ' Dim rand As New Random(&H19831983)
        Dim rand As New Random(Environment.TickCount)
        Dim ok As New State()
        Dim st As New State()
        Calc(ok)
        ok.CopyAllTo(st)
        Dim tmps(10) As State
        For i As Integer = 0 To UBound(tmps)
            tmps(i) = New State()
        Next i
        For k As Integer = 1 To 100
            Dim time1 As Integer = Environment.TickCount - startTime
            If time1 > LimitTime Then
                Console.Error.WriteLine("time limit")
                Exit For
            End If
            
            Dim flag As Boolean = True
            For i As Integer = 0 To 4
                Console.Error.WriteLine("k: {0}, i: {1}", k, i)
                Dim c As Integer = 0
                Select Case i
                Case 0
                    For j As Integer = 0 To 9
                        Dim x As Integer = st.MinBorder + If(j < 5, j - 5, j - 4)
                        If x < 0 OrElse x > 100 Then Continue For
                        st.CopyTo(tmps(c))
                        tmps(c).MinBorder = x
                        Calc(tmps(c))
                        c += 1
                    Next j
                Case 1
                    For j As Integer = 0 To 9
                        Dim x As Integer = st.BorderRate + If(j < 5, j - 5, j - 4)
                        If x < 0 OrElse x > 100 Then Continue For
                        st.CopyTo(tmps(c))
                        tmps(c).BorderRate = x
                        Calc(tmps(c))
                        c += 1
                    Next j
                Case 2
                    For j As Integer = 0 To 9
                        Dim x As Integer = st.AtariGosa + If(j < 5, j - 5, j - 4)
                        If x < 0 OrElse x > 100 Then Continue For
                        st.CopyTo(tmps(c))
                        tmps(c).AtariGosa = x
                        Calc(tmps(c))
                        c += 1
                    Next j
                Case 3
                    For j As Integer = 0 To 9
                        Dim x As Integer = st.OkRate + If(j < 5, j - 5, j - 4)
                        If x < 0 OrElse x > 100 Then Continue For
                        st.CopyTo(tmps(c))
                        tmps(c).OkRate = x
                        Calc(tmps(c))
                        c += 1
                    Next j
                Case 4
                    For j As Integer = 0 To 9
                        Dim x As Integer = st.BureRate + If(j < 5, j - 5, j - 4)
                        If x < 0 OrElse x > 100 Then Continue For
                        st.CopyTo(tmps(c))
                        tmps(c).BureRate = x
                        Calc(tmps(c))
                        c += 1
                    Next j
                End Select
                Dim sel As Integer = 0
                For j As Integer = 0 To c
                    tmps(j).Calc(maxScores)
                    If tmps(j).Score > tmps(sel).Score Then
                        sel = j
                    End If
                Next j
                st.Calc(maxScores)
                If tmps(sel).Score > st.Score Then
                    Dim swp As State = st
                    st = tmps(sel)
                    tmps(sel) = swp
                    ok.Calc(maxScores)
                    If st.Score > ok.Score Then
                        st.CopyAllTo(ok)
                        Console.Error.WriteLine("update!")
                    End If
                    flag = False
                End If
            Next i
            If flag Then
                st.MinBorder  = rand.Next(1, MaxRound + 1)
                st.BorderRate = rand.Next(10, 91)
                st.AtariGosa  = rand.Next(10, 91)
                st.OkRate     = rand.Next(10, 91)
                st.BureRate   = rand.Next(10, 91)
                Calc(st)
                Console.Error.WriteLine("random: {0}", _
                    ToTimeStr(Environment.TickCount - startTime))
            End If
        Next k
        Console.Error.WriteLine("({0}, {1}), ({2}, {3})", _
            MinN, MaxN, MinRound, MaxRound)
        Console.Error.WriteLine(ok.ToString())
    End Sub
    
    Const SeedBase As Integer = 3 * 7 * 11 * 13 * 17
    Sub Calc(st As State)
        For i As Integer = 0 To TCCount - 1
            Dim seed As Integer = SeedBase + i
            Dim iav As New InvestmentAdvice()
            st.CopyTo(iav)
            Dim score As Integer = Simurate(seed, iav)
            st.TCScores(i) = score
            If score > maxScores(i) Then
                maxScores(i) = score
            End If
        Next i
    End Sub
    
    Const InvestingLimit As Integer = 400000
    
    Const MinN As Integer = 10
    Const MaxN As Integer = 50
    Const MinRound As Integer = 67
    Const MaxRound As Integer = 100
    
    Function Simurate(seed As Integer, iav As InvestmentAdvice) As Integer
        Simurate = -1
        Dim rand As New Random(seed)
        Dim N As Integer = rand.Next(MinN, MaxN + 1)
        Dim accuracy(N - 1) As Double
        Dim actual(N - 1) As Double
        Dim stDev(N - 1) As Double
        Dim result(N - 1) As Integer
        Dim reported(N - 1) As Integer
        Dim round As Integer = rand.Next(MinRound, MaxRound + 1)
        For i As Integer = 0 To N - 1
            stDev(i) = rand.NextDouble() * 20.0
            accuracy(i) = rand.NextDouble()
        Next i
        Dim money As Integer = 1000000
        
        For r As Integer = 0 To round - 1
            For i As Integer = 0 To N - 1
                actual(i) = Math.Min(1.0, Math.Max(-1.0, NextGaussian(rand) * 0.1))
                If rand.nextDouble() < accuracy(i) Then
                    reported(i) = CInt(Math.Round( _
                        Math.Min(100.0, Math.Max(-100.0, _
                            NextGaussian(rand) * stDev(i) _
                            + actual(i) * 100.0))))
                Else
                    reported(i) = CInt(Math.Round( _
                        100.0 * Math.Min(1.0, Math.Max(-1.0, _
                            NextGaussian(rand) * 0.1))))
                End If
            Next i
            
            Dim ret() As Integer = iav.getInvestments(reported, result, money, 0, round - r)
            
            Dim total As Integer = 0
            Dim gain As Integer = 0
            For i As Integer = 0 To N - 1
                total += ret(i)
                If ret(i) > InvestingLimit Then
                    Console.Error.WriteLine("ERROR: seed={0} over ret(i)", seed)
                    Exit Function
                End If
                result(i) = CInt(Math.Floor(actual(i) * CDbl(ret(i))))
                gain += result(i)
            Next i
            If total > money Then
                Console.Error.WriteLine("ERROR: seed={0} over money", seed)
                Exit Function
            End If
            money += gain
        Next r
        
        Simurate = money
        
    End Function

	' ---------------------------------------------------------------------------------
	' randomly from a normal distribution with mean 0 and variance 1
	' references:
	'  http://docs.oracle.com/javase/jp/8/docs/api/java/util/Random.html#nextGaussian--
	'  https://en.wikipedia.org/wiki/Marsaglia_polar_method
	Dim nextNextGaussian As Double = 0.0
	Dim haveNextNextGaussian As Boolean = False
	Function NextGaussian(rand As Random) As Double
		If haveNextNextGaussian Then
			haveNextNextGaussian = False
			NextGaussian = nextNextGaussian
			Exit Function
		End If
		Dim v1 As Double, v2 As Double, s As Double
		Do
			v1 = 2.0 * rand.nextDouble() - 1.0
			v2 = 2.0 * rand.nextDouble() - 1.0
			s = v1 * v1 + v2 * v2
		Loop While s >= 1.0 OrElse s = 0.0
		Dim multiplier As Double = Math.Sqrt(-2.0 * Math.Log(s) / s)
		nextNextGaussian = v2 * multiplier
		haveNextNextGaussian = True
		NextGaussian = v1 * multiplier
	End Function
    
End Module