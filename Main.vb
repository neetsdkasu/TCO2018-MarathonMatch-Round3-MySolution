Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On
Imports Console = System.Console

Public Module Main

	Public Sub Main()
		Try
            Dim iav As New InvestmentAdvice()
            Do
                Dim A As Integer = CInt(Console.ReadLine())
                Dim advice(A - 1) As Integer
                For i As Integer = 0 To UBound(advice)
                    advice(i) = CInt(Console.ReadLine())
                Next i
                Dim R As Integer = CInt(Console.ReadLine())
                Dim recent(R - 1) As Integer
                For i As Integer = 0 To UBound(recent)
                    recent(i) = CInt(Console.ReadLine())
                Next i
                Dim money As Integer = CInt(Console.ReadLine())
                Dim timeLeft As Integer = CInt(Console.ReadLine())
                Dim roundsLeft As Integer = CInt(Console.ReadLine())
                
                Dim ret() As Integer = iav.getInvestments(advice, recent, money, timeLeft, roundsLeft)
                Console.WriteLine(ret.Length)
                For Each v As Integer In ret
                    Console.WriteLine(v)
                Next v
                Console.Out.Flush()
                
                If roundsLeft <= 1 Then
                    Exit Do
                End If
            Loop
		Catch Ex As Exception
			Console.Error.WriteLine(ex)
		End Try
	End Sub

End Module