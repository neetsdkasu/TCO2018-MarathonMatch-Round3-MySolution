Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On

Imports System
Imports System.Collections.Generic
Imports Tp = System.Tuple(Of Integer, Integer)
Imports Tp3 = System.Tuple(Of Integer, Integer, Integer)

Public Class InvestmentAdvice
    Dim startTime As Integer, limitTime As Integer
	Dim rand As New Random(19831983)
	
    Const InvestingLimit As Integer = 400000
    
    Dim border As Integer = 0
    
    Public Sub New()
        startTime = Environment.TickCount
        limitTime = startTime + 9800
		' Console.Error.WriteLine("{0}, {1}", limitTime, rand.Next(0, 100))
    End Sub
    
    Dim yosou() As Integer
    Dim bet() As Integer
    Dim atari() As Integer
    Dim hazure() As Integer
    Dim gosa() As Integer
    Dim count As Integer = 0
    
    Public MinBorder  As Integer = 1
    Public BorderRate As Integer = 40
    Public AtariGosa  As Integer = 53
    Public OkRate     As Integer = 31
    Public BureRate   As Integer = 73
    
    Public patterns() As Pattern = { _
        New Pattern(10, 25, 10,  33,  3, 17, 54, 75, 48), _
        New Pattern(26, 50, 10,  33, 20, 14, 52, 40, 77), _
        New Pattern(10, 50, 10,  33,  3, 75, 86, 19, 68), _
        New Pattern(10, 25, 34,  66, 50,  6, 59, 50, 32), _
        New Pattern(26, 50, 34,  66, 50,  0, 57, 50, 48), _
        New Pattern(10, 50, 34,  66, 50,  0, 54, 50, 45), _
        New Pattern(10, 25, 67, 100, 81,  0, 66, 67, 19), _
        New Pattern(26, 50, 67, 100, 81,  0, 62, 65, 37), _
        New Pattern(10, 50, 67, 100, 81,  0, 57, 65, 32), _
        New Pattern(10, 50, 10, 100, 50,  3, 57, 50, 42) _
    }
    
	Public Function getInvestments( _
                advice() As Integer, _
                recent() As Integer, _
                money As Integer, _
                timeLeft As Integer, _
                roundsLeft As Integer) As Integer()
		
        Dim N As Integer = advice.Length

		Dim ret(N - 1) As Integer
        getInvestments = ret
        
        count += 1
        
        If border = 0 Then
            For Each pt As Pattern In patterns
                If pt.InRange(N, roundsLeft) Then
                    pt.CopyTo(Me)
                    Exit For
                End If
            Next pt
            border = Math.Max(1, Math.Min(MinBorder, roundsLeft * BorderRate \ 100))
            ReDim yosou(N - 1), bet(N - 1)
            ReDim atari(N - 1), gosa(N - 1), hazure(N - 1)
            Dim c As Integer = 0
            Dim u As Integer = 0
            For i As Integer = 0 To N - 1
                If money < 100 Then Exit For
                bet(i) =  100
                money -= bet(i)
                If advice(i) > 0 Then
                    c += 1
                    u += advice(i)
                End If
            Next i
            For i As Integer = 0 To N - 1
                If c > 1 AndAlso advice(i) > 0 Then
                    bet(i) = Math.Min(InvestingLimit, bet(i) + money * advice(i) \ u)
                End If
                yosou(i) = advice(i)
            Next i
        ElseIf count < border Then
            For i As Integer = 0 To N - 1
                If bet(i) = 0 Then Continue For
                Dim kekka As Integer = recent(i) * 100 \ bet(i)
                Dim diff As Integer = Math.Abs(kekka - yosou(i))
                gosa(i) += diff
                If diff < AtariGosa Then
                    atari(i) += 1
                Else
                    hazure(i) += 1
                End If
            Next i
            Dim c As Integer = 0
            Dim u As Integer = 0
            For i As Integer = 0 To N - 1
                If money < 100 Then Exit For
                bet(i) =  100
                money -= bet(i)
                If advice(i) > 0 Then
                    c += 1
                    u += advice(i)
                End If
            Next i
            For i As Integer = 0 To N - 1
                If c > 1 AndAlso advice(i) > 0 Then
                    bet(i) = Math.Min(InvestingLimit, bet(i) + money * advice(i) \ u)
                End If
                yosou(i) = advice(i)
            Next i
        Else
            ' Go Go Go
            For i As Integer = 0 To N - 1
                If bet(i) = 0 Then Continue For
                Dim kekka As Integer = recent(i) * 100 \ bet(i)
                Dim diff As Integer = Math.Abs(kekka - yosou(i))
                gosa(i) += diff
                If diff < AtariGosa Then
                    atari(i) += 1
                Else
                    hazure(i) += 1
                End If
            Next i
            Dim xs As New List(Of Tp3)()
            Dim u As Integer = 0
            For i As Integer = 0 To N - 1
                bet(i) = 0
                If atari(i) = 0 Then Continue For
                Dim total As Integer = atari(i) + hazure(i)
                Dim rate As Integer = 100 * atari(i) \ total
                Dim bure As Integer = gosa(i) \ total
                If rate < OkRate Then Continue For
                Dim gain As Integer = advice(i) - bure * BureRate \ 100
                If gain < 1 Then Continue For
                u -= gain
                xs.Add(New Tp3(-gain, -rate, i))
            Next i
            xs.Sort()
            For i As Integer = 0 To xs.Count - 1
                Dim k As Integer = xs(i).Item3
                bet(k) = money * xs(i).Item1 \ u
                bet(k) = Math.Min(InvestingLimit, bet(k))
                yosou(k) = advice(k)
            Next i
        End If

        ' If roundsLeft = 1 Then
            ' Console.Error.WriteLine("count: {0}", count)
            ' For i As Integer = 0 To N - 1
                ' Console.Error.WriteLine("{0,2:D} {1,2:D} {3,2:D} {2,4:D}", _
                    ' i, atari(i), gosa(i), hazure(i))
            ' Next i
        ' End If
        
        Array.Copy(bet, ret, ret.Count)
		
	End Function
    
    Class Pattern
        Public ReadOnly MinBorder  As Integer
        Public ReadOnly BorderRate As Integer
        Public ReadOnly AtariGosa  As Integer
        Public ReadOnly OkRate     As Integer
        Public ReadOnly BureRate   As Integer
        Public ReadOnly lbN As Integer
        Public ReadOnly ubN As Integer
        Public ReadOnly lbR As Integer
        Public ReadOnly ubR As Integer
        Sub New(lbN As Integer, ubN As Integer, lbR As Integer, ubR As Integer, _
                MinBorder As Integer, BorderRate As Integer, AtariGosa As Integer, _
                OkRate As Integer, BureRate As Integer)
            Me.lbN = lbN
            Me.ubN = ubN
            Me.lbR = lbR
            Me.ubR = ubR
            Me.MinBorder  = MinBorder
            Me.BorderRate = BorderRate
            Me.AtariGosa  = AtariGosa
            Me.OkRate     = OkRate
            Me.BureRate   = BureRate
        End Sub
        Function InRange(N As Integer, R As Integer) As Boolean
            InRange = (lbN <= N AndAlso N <= ubN) _
                AndAlso (lbR <= R AndAlso R <= ubR)
        End Function
        Sub CopyTo(s As InvestmentAdvice)
            s.MinBorder  = MinBorder
            s.BorderRate = BorderRate
            s.AtariGosa  = AtariGosa
            s.OkRate     = OkRate
            s.BureRate   = BureRate
        End Sub
    End Class
End Class